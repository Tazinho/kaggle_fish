library(mxnet)    # neural network package
library(ggplot2)  # plotting library
library(Hmisc)    # useful for multi histogram
library(dplyr)    # data manipulation
library(nnet)     # for class.ind

# copied the mx.metric.mlogloss calculation from gmilosev R & mxnet script in the state farm competition
# helper function
# normalizes log loss preds so that sum is always 1
# probably not needed here, but anyway
mLogLoss.normalize = function(p, min_eta=1e-15, max_eta = 1.0){
  #min_eta
  for(ix in 1:dim(p)[2]) {
    p[,ix] = ifelse(p[,ix]<=min_eta,min_eta,p[,ix]);
    p[,ix] = ifelse(p[,ix]>=max_eta,max_eta,p[,ix]);
  }
  #normalize
  for(ix in 1:dim(p)[1]) {
    p[ix,] = p[ix,] / sum(p[ix,]);
  }
  return(p);
}

# helper function
#calculates logloss
mlogloss = function(y, p, min_eta=1e-15,max_eta = 1.0){
  class_loss = c(dim(p)[2]);
  loss = 0;
  p = mLogLoss.normalize(p,min_eta, max_eta);
  for(ix in 1:dim(y)[2]) {
    p[,ix] = ifelse(p[,ix]>1,1,p[,ix]);
    class_loss[ix] = sum(y[,ix]*log(p[,ix]));
    loss = loss + class_loss[ix];
  }
  #return loss
  return (list("loss"=-1*loss/dim(p)[1],"class_loss"=class_loss));
}

# mxnet specific logloss metric
mx.metric.mlogloss <- mx.metric.custom("mlogloss", function(label, pred){
  p = t(pred);
  m = mlogloss(class.ind(label),p);
  gc();
  return(m$loss);
})

train=read.csv("../input/train.csv",header=T,stringsAsFactors = F)
test=read.csv("../input/test.csv",header=T,stringsAsFactors = F)
#str(train)              # variable listing
#summary(train)          # gives the number of NA's
#hist.data.frame(full)   # histogram of each variable
#pairs(full)             # pairwise scatter plots

# should sample here to have a validation data set

train.x=train[,2:94]
train.y.ind=which(class.ind(train$target)==1,arr.ind=T)
train.y=train.y.ind[,2]-1
#train.x.eval=train[50001:nrow(train),2:94]
#train.y.eval=model.matrix(~ train$target[50001:nrow(train)])
test.x=test[,2:94]

# defining the network
data = mx.symbol.Variable('data')
fc1 = mx.symbol.FullyConnected(data=data, num_hidden=64)
act1 = mx.symbol.Activation(data=fc1, act_type="relu")
fc2 = mx.symbol.FullyConnected(data=act1, num_hidden=32)
act2 = mx.symbol.Activation(data=fc2, act_type="relu")
fc3 = mx.symbol.FullyConnected(data=act2, num_hidden=9)
net = mx.symbol.SoftmaxOutput(data=fc3)

# train
device = mx.cpu();
model <- mx.model.FeedForward.create(
  X                  = t(as.matrix(train.x)),
  y                  = train.y,
  #  eval.data          = list("data"=t(as.matrix(train.x.eval)),"label"=train.y.eval),
  ctx                = device,    # device is either the cpu or gpu (graphical processor unit)
  symbol             = net,       # this is the network structure
  eval.metric        = mx.metric.mlogloss,
  #val.metric        = mx.metric.accuracy,
  num.round          = 100,       # how many batches to work with
  learning.rate      = 1e-2,      # 0.01 is a good start
  momentum           = 0.9,       # using second derivative
  wd                 = 0.0001,    # what is this for?
  initializer        = mx.init.normal(1/sqrt(nrow(train.x))),   # the standard devation is scaled with the number of
  #observations to prevent slow learning if by chance all weights are large or small
  #initializer        = mx.init.uniform(0.1),   # the standard devation is scaled with the number of 
  array.batch.size   = 500,
  #epoch.end.callback = mx.callback.save.checkpoint("titanic"),
  #batch.end.callback = mx.callback.log.train.metric(100),
  array.layout="colmajor"
);

test.y=predict(model, as.matrix(test.x))
#summary(test.y)

#graph.viz(model$symbol$as.json())
#preds = predict(model, as.matrix(test.x))
#ggplot(data=data.frame(preds[1,]), aes(preds[1,])) +    # check the distribution of the predictions
# geom_histogram(fill = "red", alpha = 0.2) 

# attach to data.frame
testprwid=data.frame(t(test.y))
colnames(testprwid)=c("Class_1","Class_2","Class_3","Class_4","Class_5","Class_6","Class_7","Class_8","Class_9")
testpr=data.frame(id=test$id,testprwid)
rownames(testpr) = NULL
# write to csv-file
write.csv(testpr,file="otto_mxnet_subm.csv",quote=FALSE,row.names=FALSE)